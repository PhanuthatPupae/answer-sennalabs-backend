# For Question 5 - 7

## Question 5

### Download sqlite [sqlite](https://www.sqlite.org/download.html) <br />
1. write command cd directory files .open cars <br />
2. CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT,name text);<br />
3. CREATE TABLE cars (id INTEGER PRIMARY KEY AUTOINCREMENT,model text,brand text,user_id INTEGER REFERENCES userd(id));<br />
4. write command. .table <br />
5. INSERT INTO users (name) VALUES ('Risk'),('John'),('Zing'),('Nan'); <br />
6. write command. select * from users; <br />
7. INSERT INTO cars (brand,model,user_id) VALUES ('Corvette','Z06',1),('Lotus','Exige S',1),('BMW','M3',1);<br />
8. INSERT INTO cars (brand,model,user_id) VALUES ('BMW','320d',2),('Mercedes','SLK AMG',2);<br />
9. INSERT INTO cars (brand,model,user_id) VALUES ('Toyota','Alphard',3),('Mercedes','Sprinter',3);<br />
10. INSERT INTO cars (brand,model,user_id) VALUES ('Toyota','Camry',4),('BMW','M5',4),('Porsche','911',4),('Jaguar','',4),('TukTuk','',4),('Mini','Cooper',4),('Honda','Jazz',4);<br />
11. select * from cars
### Result
12. select u.name , c.brand ,c.model from cars as c left join users as u where c.user_id = u.id; 
## Question 6

* select u.name ,count(c.id) from cars as c left join users as u where u.id = c.user_id group by u.name order by u.name;

## Question 7

* select u.name ,count(c.id) from cars as c left join users as u where u.id = c.user_id group by u.name having count(c.id)>2 order count(c.id);
